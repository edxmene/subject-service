import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class CachingService {
  usersBS = new BehaviorSubject<any[]>([{ id: 0, name: '' }]);
  users: any[] = [];
  user = new BehaviorSubject<any>({});
  message = new Subject<string>();

  constructor(private http: HttpClient) {}

  updateCache(id: number) {
    this.http
      .get('https://jsonplaceholder.typicode.com/users/' + id.toString())
      .subscribe((data: any) => {
        this.users.push(data);
        this.usersBS.next(this.users);
      });
  }

  cleanCache() {
    this.users = [];
    this.usersBS.next([]);
    this.message.next('Cache cleaned');
  }

  getUser(id: number) {
    if (!this.users.some((user) => user.id === +id)) {
      this.updateCache(id);
      this.message.next('Requesting on API...');
    } else {
      this.message.next('Requesting on Cache...');
    }
    this.usersBS.subscribe((users) => {
      const user_ = users.filter((user) => {
        return user.id == id;
      });
      this.user.next(user_);
    });
  }
}

//When I have a subscription in a service, how do I unsubscribe
