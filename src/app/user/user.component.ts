import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { CachingService } from '../caching.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'],
})
export class UserComponent implements OnInit, OnDestroy {
  userIdInput: any;
  userInfo: any;
  message!: string;
  userSubscription: any;
  messageSubscription: any;
  constructor(private cachingService: CachingService) {}

  ngOnInit(): void {
    this.messageSubscription = this.cachingService.message.subscribe(
      (message) => {
        this.message = message;
      }
    );
  }

  onSearchingUser() {
    this.cachingService.getUser(this.userIdInput);
    this.userSubscription = this.cachingService.user.subscribe((user) => {
      [this.userInfo] = user;
    });
  }

  onCleanCache() {
    this.cachingService.cleanCache();
    this.userIdInput = '';
  }

  ngOnDestroy(): void {
    this.messageSubscription.unsubscribe();
    this.userSubscription.unsubscribe();
  }
}
