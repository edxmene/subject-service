import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgControl } from '@angular/forms';
import { CachingService } from './caching.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  title = 'subject-challenge';
  users: any = [];

  constructor() {}

  ngOnInit(): void {}
}
